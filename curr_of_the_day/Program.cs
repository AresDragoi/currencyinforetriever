﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace curr_of_the_day
{
    class Program
    {
        static void Main(string[] args)
        {        
            //website to fetch from
            var site = @"HTTPS://WWW.BANKOFALBANIA.ORG/TREGJET/KURSI_ZYRTAR_I_KEMBIMIT/";
            //load page html
            var web = new HtmlWeb();
            var htmldoc = web.Load(site);
 
            GetEachNode(htmldoc);

            Console.WriteLine();
        }
        static void GetEachNode(HtmlDocument thedoc)
        {
            Dictionary<string, decimal> dict = new Dictionary<string, decimal>();
            var symbolnodes = thedoc.DocumentNode.SelectNodes("//td[position()=2]");
            var nrnodes = thedoc.DocumentNode.SelectNodes("//td[position()=3]");

            //fill the list
            int c = 0; int i = 1; 
            while(c!=symbolnodes.Count)
            {
                string s = symbolnodes[c].GetDirectInnerText();
                decimal n = decimal.Parse(nrnodes[c].GetDirectInnerText());

                dict.Add(s, n);
                c++; 
                
             }
            //print the list
            foreach (KeyValuePair<string, decimal> text in dict)
            {
                Console.WriteLine("{0} {1}", text.Key, text.Value);
                Console.WriteLine();
            }
        }

    }
}
